package com.setter;

public class SetterEditor {
    private SetterChecker setterChecker;

    // a setter method to inject the dependency.
    public void setSetterChecker(SetterChecker setterChecker) {
        System.out.println("Inside setSetterChecker." );
        this.setterChecker = setterChecker;
    }
    // a getter method to return spellChecker
    public SetterChecker getSetterChecker() {
        return setterChecker;
    }

    public void spellCheck() {
        setterChecker.checkSpelling();
    }
}
