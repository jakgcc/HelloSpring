package com.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainAPP {
    public static void main(String[] args) {
//	      ApplicationContext context =
//	             new ClassPathXmlApplicationContext("Beans.xml");
        AbstractApplicationContext context =
                new ClassPathXmlApplicationContext("Beans.xml");
        HelloWorld obj = (HelloWorld) context.getBean("helloWorld");
        obj.getMessage();
        context.registerShutdownHook();
//	      HelloWorld objA = (HelloWorld) context.getBean("helloWorld");
//	      objA.setMessage("I'm object A");
//	      objA.getMessage();
//
//	      HelloWorld objB = (HelloWorld) context.getBean("helloWorld");
//	      objB.getMessage();
    }

}
