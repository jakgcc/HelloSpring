package com.Event;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainEvent {
    public static void main(String[] args) {
        ConfigurableApplicationContext context =
                new ClassPathXmlApplicationContext("Beans.xml");

        // Let us raise a start event.
        context.start();

        HelloEvent obj = (HelloEvent) context.getBean("helloEvent");

        obj.getMessage();

        // Let us raise a stop event.
        context.stop();
    }
}
