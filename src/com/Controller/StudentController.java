package com.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
@Controller
public class StudentController {
    @RequestMapping(value = "/student", method = RequestMethod.GET)
    public ModelAndView student() {
        return new ModelAndView("student", "command", new StudentView());
    }

    @RequestMapping(value = "/addStudent", method = RequestMethod.POST)
    @ExceptionHandler({SpringException.class})
    public String addStudent(@ModelAttribute("springWeb")StudentView studentView,
                             ModelMap model) {
        if(studentView.getName().length() < 5 ){
            throw new SpringException("Given name is too short");
        }else{
            model.addAttribute("name", studentView.getName());
        }
        if( studentView.getAge() < 10 ){
            throw new SpringException("Given age is too low");
        }else{
            model.addAttribute("age", studentView.getAge());
        }
        model.addAttribute("id", studentView.getId());

        return "result";
    }
}
