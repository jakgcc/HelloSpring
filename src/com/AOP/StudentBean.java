package com.AOP;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class StudentBean {
    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("Beans.xml");

        StudentView studentView = (StudentView) context.getBean("studentView");

        studentView.getName();
        studentView.getAge();

        studentView.printThrowException();
    }
}
